import { Injectable } from '@angular/core';
import  { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }


  register (userDetails) {
    console.log(userDetails);
     return this.http.post('http://localhost:3002/signup', userDetails );

  }

  login (userDetails) {
    console.log(userDetails);
    return this.http.post('http://localhost:3002/login', userDetails);
  
  }

  getDetails(){
    return this.http.get('http://localhost:3002/getUsers');
  }

  getDetailsUsingId(id){
    console.log(id);
    return this.http.get(`http://localhost:3002/details?id=${id}`);
  }

  updateDetailsUsingId(id,updatedata){
    return this.http.patch(`http://localhost:3002/updateUser?id=${id}`, updatedata);
  }

  DeleteUser(id){
    return this.http.delete(`http://localhost:3002/deleteUser?id=${id}`);
  }

    getUsers() {
     return this.http.get('http://localhost:3002/getUsers');
    }
  }
