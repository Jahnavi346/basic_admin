import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  registerArray: any = {};
  error: any;
  constructor(private router: Router, private userService: UserService,private Toastr:ToastrService) { }

    ngOnInit() {
    }
    onRegister(data){
      console.log(data);
    const reqObj: any = {
      name: data.userid,
      email: data.emailid,
      password: data.password
    }
   this.userService.register(reqObj).subscribe((res) => {
      console.log(res);
      if (res) {
        this.Toastr.success('Registration Completed','',  {timeOut: 3000});
        this.router.navigateByUrl('/login');
      }
    }, err => {
      this.Toastr.error(err.error,'Error',  {timeOut: 3000});
    });   
    
  }
      loginFunction () {
        this.router.navigateByUrl('/login');
    }
    }
