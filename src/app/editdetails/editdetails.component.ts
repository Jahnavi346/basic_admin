import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router, RouterModule } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-editdetails',
  templateUrl: './editdetails.component.html',
  styleUrls: ['./editdetails.component.scss']
})
export class EditdetailsComponent implements OnInit {
  editDetails: any = {};
  updateObj: any = {};

  constructor(private userService: UserService,private router: Router,private activatedrouter:ActivatedRoute,private Toastr:ToastrService) { }

  ngOnInit() {
    console.warn(this.activatedrouter.snapshot.params.id);
    this.userService.getDetailsUsingId(this.activatedrouter.snapshot.params.id).subscribe((res) => {
      this.editDetails = res;
    });
    

  }

  onUpdate(fieldName) {
    this.updateObj[fieldName] = this.editDetails[fieldName];
  }

  updateDetails() {
    this.userService.updateDetailsUsingId(this.activatedrouter.snapshot.params.id, this.updateObj).subscribe((res) => {
      console.log(res, "data updated successfully");
      if(res){
      this.Toastr.success('Updated Successful','', {timeOut: 3000});
      }
    }, err => {
      console.log(err.error);
      this.Toastr.error(err.error,'Error',  {timeOut: 3000});
    }); 
  }


}