import { NgModule } from '@angular/core';
import { RangeValueAccessor } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { GetdetailsComponent } from './getdetails/getdetails.component';

import { UsersListComponent } from './users-list/users-list.component';
import { EditdetailsComponent } from './editdetails/editdetails.component';

const routes: Routes = [
  {
    path:'',
  component:LoginComponent,
  
  },
  {
    path:'registration',
  component: RegistrationComponent
  },
  {
    path: 'UserDetails',
    component: GetdetailsComponent
  },
  {
    path: 'UpdateDetails/:id',
    component: EditdetailsComponent
  },
  
  {
    path: '**',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
