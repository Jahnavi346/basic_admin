import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm : FormGroup

  constructor(private router: Router,private userService: UserService,private Toastr:ToastrService) {
    this.loginForm=new FormGroup({
    email:new FormControl('',[Validators.required ,Validators.email]),
    password:new FormControl('',[Validators.required,Validators.minLength(6)])
    });

   }

  ngOnInit() {
  
  }
  onSubmit(data){
      console.warn(data);
      const reqObj: any = {
        email: data.email,
        password: data.password
      }
     this.userService.login(reqObj).subscribe((res) => {
        console.log(res);
        if (res) {
          this.Toastr.success('Login Successful','', {timeOut: 3000});
          this.router.navigateByUrl('/UserDetails');
        }
      }, err => {
        console.log(err.error);
        this.Toastr.error(err.error,'Error',  {timeOut: 3000});
      }); 

  }
  registrationFunction () {
    this.router.navigateByUrl('/registration');
  }

}
