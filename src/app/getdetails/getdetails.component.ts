import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router, RouterModule } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-getdetails',
  templateUrl: './getdetails.component.html',
  styleUrls: ['./getdetails.component.scss']
})
export class GetdetailsComponent implements OnInit {
  data:any;
  p = 1;
  constructor(private userService: UserService,private router: Router,private activatedrouter:ActivatedRoute,private Toastr:ToastrService) { }

  ngOnInit() {
    this.userService.getDetails().subscribe((res: any) => {
      console.warn(res);
      this.data=res;
      let count = 1;
      this.data.forEach((obj) => {
        obj.serialNo = count;
        count++;
      });
    });

  }
  EditFunction (id) {
    this.router.navigateByUrl(`/UpdateDetails/${id}`);
  }

  DeleteFunction (id) {
    this.userService.DeleteUser(id).subscribe((res) => {
      console.log(res,"delete");
      if(res){
        this.Toastr.success('Deleted Successful','', {timeOut: 3000});
        }
    }, err => {
      console.log(err.error);
      this.Toastr.error(err.error,'Error',  {timeOut: 3000});
    });
    
  }


}
